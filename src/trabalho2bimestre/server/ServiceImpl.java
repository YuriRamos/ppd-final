/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho2bimestre.server;

import trabalho2bimestre.client.Janela;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Scanner;
import trabalho2bimestre.client.Cliente;
import trabalho2bimestre.client.Service;

/**
 *
 * @author aluno
 */
public class ServiceImpl extends UnicastRemoteObject implements Service {

    private String texto;
    private boolean modified = false;
    private ArrayList<Janela> clientes = new ArrayList();

    protected ServiceImpl() throws RemoteException {
        super();
    }

    private static final long serialVersionUID = 1L;

    @Override
    public void novoCliente(Janela novo) throws Exception {
        clientes.add(novo);
    }

    @Override
    public String atualizarTexto(String newTexto) throws Exception {
        this.setTexto(newTexto);
        System.out.println(newTexto);
        setModified(true);
        return this.getTexto();
    }

    @Override
    public String iniciarTexto() throws Exception {
        return texto;
    }

    /**
     * @return the texto
     */
    public synchronized String getTexto() {
        return texto;
    }

    /**
     * @param texto the texto to set
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Override
    public synchronized String getTextoString() throws Exception {
        this.modified = false;
        return texto.toString();
    }

    /**
     * @return the modified
     */
    public boolean isModified() throws Exception {
        return modified;
    }

    @Override
    public void setModified(boolean modified) throws Exception {
        this.modified = modified;
    }

    /**
     * @param modified the modified to set
     */
}
