/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho2bimestre.server;

import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


/**
 *
 * @author aluno
 */
public class Server{
    private static ServiceImpl service;
    public static void main(String[] args) {
		try {
                        System.setProperty("java.rmi.server.hostname","10.20.24.119");
			//Cria um novo serviço
                        Server.setService(new ServiceImpl());
			
			//Define o nome do objeto servidor que será utilizado por clientes
			String loc = "//10.20.24.119:1099/service";
			
			//Registra o serviço no servidor, passando por parâmetro 
			//o nome do serviço a ser disponibilizado, e o objeto remoto
			Naming.rebind(loc, getService());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    /**
     * @return the service
     */
    public static ServiceImpl getService() {
        return service;
    }

    /**
     * @param aService the service to set
     */
    public static void setService(ServiceImpl aService) {
        service = aService;
    }
}
