/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho2bimestre.client;
import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 *
 * @author aluno
 */
public interface Service extends Remote{
    
    public void novoCliente(Janela novo) throws Exception;
    public String atualizarTexto(String newTexto) throws Exception;
    public String iniciarTexto() throws Exception;
    public String getTextoString() throws Exception;
    public boolean isModified() throws Exception;
    public void setModified(boolean modified) throws Exception;
    
}
