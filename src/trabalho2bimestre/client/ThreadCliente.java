/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho2bimestre.client;

import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import trabalho2bimestre.server.Server;
import trabalho2bimestre.server.ServiceImpl;

/**
 *
 * @author aluno
 */
public class ThreadCliente implements Runnable {

    Janela janela;
    Service service;

    ThreadCliente(Janela janela, Service service) {
        this.janela = janela;
        this.service = service;
    }

    @Override
    public void run() {
        int position = 0;
        while (true) {
            try {
                sleep(1000);
                position = janela.getTexto().getCaretPosition();
                if (janela.getTexto().toString().equals(service.getTextoString()) == false) {
                    janela.getTexto().setText(service.getTextoString());
                    janela.getTexto().setCaretPosition(position);
                }
            } catch (Exception ex) {
                
            }
        }
    }

}
