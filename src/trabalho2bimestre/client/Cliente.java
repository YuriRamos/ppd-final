/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho2bimestre.client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;
import trabalho2bimestre.server.ServiceImpl;

/**
 *
 * @author aluno
 */
public class Cliente{
    private static Service service;
    public static void main(String[] args) {

        try {
            System.setProperty("java.rmi.server.hostname","10.20.24.119");
            String loc = "//10.20.24.119/service";
            //Procura pelo servidor para obter o serviço
            setService((Service) Naming.lookup(loc));
            boolean saida = false;
            Scanner teclado = new Scanner(System.in);
            try {
                getService().novoCliente(new Janela(service));
            }catch(Exception e) {
                e.printStackTrace();
            }
            teclado.close();
        } catch (MalformedURLException | RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the service
     */
    public static Service getService() {
        return service;
    }

    /**
     * @param aService the service to set
     */
    public static void setService(Service aService) {
        service = aService;
    }
}
