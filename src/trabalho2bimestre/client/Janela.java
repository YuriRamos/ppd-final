/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho2bimestre.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import trabalho2bimestre.server.Server;
import trabalho2bimestre.server.ServiceImpl;

/**
 *
 * @author aluno
 */
public class Janela extends JFrame {

    private JTextPane texto;
    private Service service;
    private boolean atualizado = true;
    Observable server;

    public Janela(Service service) {
        this.service = service;
        texto = new JTextPane();

        Thread notificadora = new Thread();
        Thread listener = new Thread(new ThreadCliente(this, this.service));
        notificadora.start();
        listener.start();
        try {
            texto.setText(service.iniciarTexto());
        } catch (Exception ex) {
            System.out.println("Erro");
        }
        texto.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                try {
                    if (texto.getText() != "") {
                        System.out.println(service.atualizarTexto(texto.getText()));
                        service.setModified(true);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                try {
                    if (texto.getText() != "") {
                        System.out.println(service.atualizarTexto(texto.getText()));
                        service.setModified(true);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override

            public void changedUpdate(DocumentEvent e) {
                System.out.println("changedUpdate");
            }

        }
        );

        this.add(texto);
        this.setTitle("Editor de Texto Colaborativo");
        this.setSize(400, 300);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    /**
     * @return the texto
     */
    public synchronized JTextPane getTexto() {
        return texto;
    }

    /**
     * @param texto the texto to set
     */
    public synchronized void setTexto(JTextPane texto) {
        this.texto = texto;
    }

    /**
     * @return the service
     */
    public Service getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(Service service) {
        this.service = service;
    }

    /**
     * @return the atualizado
     */
    public boolean isAtualizado() {
        return atualizado;
    }

    /**
     * @param atualizado the atualizado to set
     */
    public void setAtualizado(boolean atualizado) {
        this.atualizado = atualizado;
    }

}
